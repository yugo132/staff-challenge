# Generated by Django 2.0 on 2018-03-15 21:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='is_active')),
                ('type_product', models.CharField(choices=[('GC', 'Gift Card'), ('SR', 'Service'), ('PR', 'Product')], max_length=2, verbose_name='type_product')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('is_variation', models.BooleanField(default=False, verbose_name='is_variation')),
                ('brand_id', models.PositiveIntegerField(blank=True, null=True, verbose_name='brand_id')),
                ('code', models.CharField(max_length=50, verbose_name='code')),
                ('family', models.IntegerField(verbose_name='family')),
                ('is_complement', models.BooleanField(default=False, verbose_name='is_complement')),
                ('is_delete', models.BooleanField(default=False, verbose_name='is_delete')),
            ],
            options={
                'verbose_name_plural': 'Products',
                'verbose_name': 'Product',
            },
        ),
        migrations.CreateModel(
            name='ProductDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='is_active')),
                ('is_visibility', models.BooleanField(default=True, verbose_name='is_visibility')),
                ('price', models.PositiveIntegerField(verbose_name='price')),
                ('price_offer', models.PositiveIntegerField(blank=True, null=True, verbose_name='price offer')),
                ('offer_day_from', models.DateTimeField(blank=True, null=True, verbose_name='offer day from')),
                ('offer_day_to', models.DateTimeField(blank=True, null=True, verbose_name='offer day to')),
                ('quantity', models.PositiveIntegerField(verbose_name='quantity')),
                ('sku', models.PositiveIntegerField(verbose_name='sku')),
                ('product', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='products.Product')),
            ],
            options={
                'verbose_name_plural': 'Products Details',
                'verbose_name': 'Product Details',
            },
        ),
    ]
