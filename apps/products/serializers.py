
from rest_framework import serializers, status
from .models import Product, ProductDetails


class ProductDetailsSerializer(serializers.ModelSerializer):
      
      class Meta: 
        model = ProductDetails
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
      details = ProductDetailsSerializer(required=False)
      
      class Meta:
          model = Product
          fields = '__all__'

      def created(self, validated_data):
          """
          override the default create to add details to Product
          """

          details_data = validated_data.pop('details')
          product = Product.objects.create(**validated_data)
          if details_data:
              ProductDetails.objects.create(product=product, **details_data)

          return product
