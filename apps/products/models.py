from django.db import models
from django.utils.translation import ugettext_lazy as _
from base.models import TimestampMixin
# Create your models here.


class Product (TimestampMixin):
    """
        Product Model
    """
    GIFTCARD = 'GC'
    SERVICE = 'SR'
    PRODUCT = 'PR'

    TYPE_CHOICES = (
        (GIFTCARD, _("Gift Card")),
        (SERVICE,_('Service')),
        (PRODUCT, _('Product'))
    )

    is_active = models.BooleanField(_('is_active'), default=True)
    type_product = models.CharField(_('type_product'), max_length=2, choices=TYPE_CHOICES)
    name = models.CharField(_('name'), max_length=100)
    description = models.TextField(_('description'), null=True, blank=True)
    is_variation = models.BooleanField(_('is_variation'), default=False)
    brand_id = models.PositiveIntegerField(_('brand_id'), null=True, blank=True)
    code = models.CharField(_('code'), max_length=50)
    family = models.IntegerField(_('family'))
    is_complement = models.BooleanField(_('is_complement'), default=False)
    is_delete = models.BooleanField(_('is_delete'), default=False)

    class Meta:
      verbose_name = _('Product')
      verbose_name_plural = ('Products')

class ProductDetails(TimestampMixin):
    """
        Product Details Model
    """
    is_active = models.BooleanField(_('is_active'), default=True)
    is_visibility = models.BooleanField(_('is_visibility'), default=True)
    price = models.PositiveIntegerField(_('price'))
    price_offer = models.PositiveIntegerField(_('price offer'), null=True, blank=True)
    offer_day_from = models.DateTimeField(_('offer day from'), null=True, blank=True)
    offer_day_to = models.DateTimeField(_('offer day to'), null=True, blank=True)
    quantity = models.PositiveIntegerField(_('quantity'))
    sku = models.PositiveIntegerField(_('sku'))
    product = models.OneToOneField(Product, on_delete=models.CASCADE, related_name='details')

    class Meta:
      verbose_name = _('Product Details')
      verbose_name_plural = ('Products Details')
