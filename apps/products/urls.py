from django.conf import settings
from django.conf.urls import include, url
from .views import ProductList, ProductDetails
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^products/$', ProductList.as_view()),
    url(r'^products/(?P<pk>[0-9]+)/$', ProductDetails.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)